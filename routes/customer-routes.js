const express = require('express');
const { route } = require('express/lib/application');
const { 
    getAllCustomers, 
    getAddCustomerView, 
    addCustomer, 
    getUpdateCustomerView,
    updateCustomer,
    getDeleteCustomer,
    deleteCustomer } = require('../controllers/customerController');

const router = express.Router();

router.get('/', getAllCustomers);
router.get('/addCustomer', getAddCustomerView);
router.post('/addCustomer', addCustomer);
router.get('/updateCustomer/:id', getUpdateCustomerView);
router.post('/updateCustomer/:id', updateCustomer);
router.get('/deleteCustomer/:id', getDeleteCustomer);
router.post('/deleteCustomer/:id', deleteCustomer);

module.exports = {
    routes: router
}