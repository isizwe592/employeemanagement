const mongoose = require('mongoose');
const winston = require('winston');

module.exports = () => {
    mongoose.connect('mongodb://localhost/customerDB', {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(() => winston.info('Mongodb connected successfully...'));
}